import pandas as pd
import numpy as np
import scipy as sp

from typing import Union, List, Tuple, Dict

import miceforest as mf

from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler


def preprocess(df: pd.DataFrame, cat_cols, cont_cols, target_col="y", dummy_coding=True, rand_seed: int = 667,
               enc_dict=None, scaler_ord=None, scaler_cont=None, mc_kernel=None, mc_on_all_cols=False):
    print(f"Data set with {len(df)} samples")

    # transform cat variables -> and target variable
    print("Transforming categorical variables ...")
    df_pp, enc_dict = transform_categorical_cols(df, cat_cols + [target_col],
                                                 encoder_dict=enc_dict,
                                                 replace_cols=True,
                                                 nan_str="unknown",
                                                 keep_nan_str_cols=["default", "poutcome"])

    if target_col is not None:
        # keep binarised target as separate vector
        y = df_pp[target_col]

        # drop from feature df
        df_pp = df_pp.drop(columns=target_col)
    else:
        y = None

    if scaler_cont is not None:
        # scale continuous variables
        print(f"Scaling continuous variables ...")
        df_pp, scaler_cont = scale_cols(df_pp, cont_cols, scaler=scaler_cont)

    if scaler_ord is not None:
        # scale ordinal variables
        print(f"Scaling ordinal variables ...")
        df_pp, scaler_ord = scale_cols(df_pp, cat_cols, scaler=scaler_ord)

    # train mice model and impute
    print("Imputing missing values ...")
    df_pp, mc_kernel = impute_missing_vals(df_pp, kernel=mc_kernel, train_nonmissing=mc_on_all_cols, rand_seed=rand_seed, n_iter=20)

    # skip dummy coding if required
    if not dummy_coding:
        return df_pp, y, enc_dict, scaler_cont, scaler_ord, mc_kernel

    # dummy code
    print("Dummy coding categorical variables ...")
    df_pp = dummy_code(df_pp, cat_cols)

    print("Done")
    return df_pp, y, enc_dict, scaler_cont, scaler_ord, mc_kernel


def test_independence(df: pd.DataFrame, col_y: str = "y", col_x: str = Union[None, str],
                      missing_vals=Union[None, List]) -> Tuple["chi_2_stats", "cont_table"]:
    """
    Use chi^2 test to test whether two given variables are independent, given their missing values.
    Assumes 'y' target variable as default.

    :param df: dataframe with columns col_y and col_x
    :param col_y: name of column with target variable
    :param col_x: name of column with feature variable
    :param missing_vals: list of values that are considered as missing if encountered in y_col or x_col
    :return: chi^2 test stats and contingency table
    """

    if missing_vals is None:
        raise ValueError("Please specify values to consider in col_x")
    elif type(missing_vals) is str:
        missing_vals = [missing_vals]

    df_tmp = df.copy()
    df_tmp["is_missing"] = None
    df_tmp.loc[df_tmp.query(f"{col_x} in @missing_vals").index, "is_missing"] = 1
    df_tmp.loc[df_tmp.query(f"{col_x} not in @missing_vals").index, "is_missing"] = 0

    ct = pd.crosstab(df_tmp["is_missing"], df_tmp[col_y])

    obs = np.array([ct.iloc[0], ct.iloc[1]])
    res = sp.stats.chi2_contingency(obs)

    return res, ct


def drop_missing(df: pd.DataFrame, cols: List[str] = None, missing_vals: List[str] = None) -> pd.DataFrame:
    """
    Drop rows that involve at least one missing value

    :param df: Dataframe
    :param cols: Columns to consider
    :param missing_vals: List with strings considered as missing values
    :return: Dataframe without dropped rows
    """

    if type(missing_vals) is str:
        missing_vals = [missing_vals] * len(cols)

    df_tmp = df.copy()
    m = np.array([False] * df_tmp.shape[0])
    for col, val in zip(cols, missing_vals):
        m = m | (df[col] == val)

    return df_tmp.loc[~m]


def transform_categorical_cols(df: pd.DataFrame, cols: List[str], encoder_dict: Union[None, Dict] = None,
                               replace_cols: bool = True, nan_str: Union[None, str] = None,
                               keep_nan_str_cols: List[str] = None) -> pd.DataFrame:
    """
    Encode categorical variables as ordinal integers.
    Code missing values as NaN if required.

    :param df: DataFrame
    :param cols: Columns to consider
    :param encoder_dict: Use previously fitted encoder, i.e. to keep mapping for test label data
    :param replace_cols: Override original columns or add new columns with prefix
    :param nan_str: String that defines missing values
    :param keep_nan_str_cols: exclude these columns from replacing missing values by NaN
    :return:
    """

    df_tmp = df.copy()

    if encoder_dict is None:
        encoder_dict = {}

    # cycle through categorical cols
    for col in cols:

        # catch testset cases where y has been replaced by None
        # TODO: clean this up!
        if col is None:
            continue

        # skip previously encoded cols
        if pd.api.types.is_numeric_dtype(df[col]):
            print(f"Skipping {col} as it is already numeric!")

        else:
            if col not in keep_nan_str_cols:
                # keep missing values:
                # do this only for columns we do not want to keep missing values as dedicated category!
                # temporarily replace missing values by most frequent category
                # -> replace by NaN below
                # get boolean mask of missing values
                m_missing = df_tmp[col] == nan_str
                tmp_str = df_tmp[col].value_counts().idxmax()
                df_tmp.loc[m_missing, col] = tmp_str

            # reshape data into required format by LabelEncoder
            tmp = np.reshape(df_tmp[col].values, -1)

            # did we provide a previously fitted encoder?
            if col in encoder_dict.keys():
                # use previously fitted encoder
                encoder = encoder_dict.get(col)
                print(f"Found existing encoder for {col}")
            else:
                # fit new encoder
                encoder = LabelEncoder().fit(tmp)

            # replace existing cols or add new cols
            if replace_cols:
                col_str = col
            else:
                col_str = f"{col}_ord"

            # encode values
            df_tmp[col_str] = encoder.transform(tmp)

            # replace previously removed nan strings by NaN
            if col not in keep_nan_str_cols:
                df_tmp.loc[m_missing, col] = np.nan

            # keep encoders
            encoder_dict[col] = encoder

    # represent ints as floats due to np.NaN and imputer problem where cols with different dtypes are not matched
    return df_tmp.astype(np.float64), encoder_dict


def scale_cols(df: pd.DataFrame, cols: List[str], scaler: Union["ScalerObject", str, None] = None) -> Tuple[
    pd.DataFrame, "ScalerObject"]:
    """
    Scale columns using zscores or minmax scaling.
    :param df: DataFrame
    :param cols: considered cols
    :param scaler: 'zscore' or 'minmax' or scaler object, fitted on previous data
    :return: scaled data and fitted scaler object
    """
    df_tmp = df.copy()

    if type(scaler) is str:
        if scaler not in ["zscore", "minmax"]:
            raise ValueError("Scaling method must be either 'zscore' or 'minmax'!")

        if scaler == "zscore":
            # fit zscore scaler
            scaler = StandardScaler().fit(df_tmp[cols])
        elif scaler == "minmax":
            scaler = MinMaxScaler().fit(df_tmp[cols])

    elif (type(scaler) is StandardScaler) or (type(scaler) is MinMaxScaler):
        if np.all(np.array(cols) == scaler.feature_names_in_):
            print(f"Using provided scaler for features: {scaler.feature_names_in_}!")
        else:
            raise ValueError(f"You provided an incompatible scaler for this dataset!")
    else:
        raise ValueError(
            "Scaler must either be 'minmax' or 'zscore' or of a pretrained scaler object of either method!")

    # scale
    df_tmp[cols] = scaler.transform(df_tmp[cols])

    return df_tmp, scaler


def dummy_code(df: pd.DataFrame, cat_cols: List[str]) -> pd.DataFrame:
    """
    Dummy code categorical columns
    Create 1-n_levels columns

    :param df: DataFrame
    :param cat_cols: Columns to consider
    :return:
    """
    df_tmp = df.copy()

    df_dummy_list = []
    for col in cat_cols:
        # cast col to int
        df_tmp[col] = df_tmp[col].astype(int)

        # get dummies
        df_dummy_list.append(pd.get_dummies(df_tmp[col], drop_first=True, dtype=int, prefix=col))

    return pd.concat(df_dummy_list + [df_tmp.drop(columns=cat_cols)], axis=1)


def impute_missing_vals(df: pd.DataFrame, kernel: "MICEkernelObject" = None, rand_seed: int = 667, n_iter: int = 20, train_nonmissing: bool=False) -> \
Union[pd.DataFrame, Tuple[pd.DataFrame, "MICEkernelObject"]]:
    """
    Impute missing values using MICEforest:
    Fast, memory efficient Multiple Imputation by Chained Equations (MICE) with lightgbm

    :param df: DataFrame
    :param kernel: previously fitted kernel, i.e. for test data
    :param rand_seed: random seed
    :param n_iter: run for n iterations
    :param train_nonmissing: train model on all features in case test set may involve missing features not encountered in training set
    :return: DataFrame with imputed data
    """

    df_tmp = df.copy()

    if kernel is None:
        # Create kernel
        kernel = mf.ImputationKernel(
            df_tmp,
            datasets=1,
            save_all_iterations=True,
            random_state=rand_seed,
            train_nonmissing=train_nonmissing # train mice model on all features -> in case test data may involve missing values in previously non-missing cols
        )

        # Run the MICE algorithm for n iterations
        kernel.mice(n_iter, verbose=False)

        # Return the completed dataset.
        return kernel.complete_data(), kernel

    else:
        # impute data with pre trained model
        return kernel.impute_new_data(df_tmp).complete_data(), None


def insert_missing_cols(df1_in: pd.DataFrame, df2_in: pd.DataFrame) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Compares columns of two DataFrames.
    Creates missing columns with 0s

    Use for feature cols that were dummy coded.

    :param df1_in:
    :param df2_in:
    :return: Either DataFrame with Union of columns
    """
    df1 = df1_in.copy()
    df2 = df2_in.copy()

    diff_cols = list(np.setdiff1d(df1.columns, df2.columns)) + list(np.setdiff1d(df2.columns, df1.columns))
    print("Found a set difference in columns between test and trainin set - filling in dummy columns ...")
    print(diff_cols)
    for col in diff_cols:
        if not col in df1.columns:
            df1[col] = 0

        if not col in df2.columns:
            df2[col] = 0

    return df1.sort_index(axis=1), df2.sort_index(axis=1)


if __name__ == "__main__":
    import pickle

    # categorical variables
    cat_cols = ["education",
                "job",
                "marital",
                "default",
                "housing",
                "loan",
                "contact",
                "month",
                "day_of_week",
                "campaign",
                "previous",
                "poutcome"
                ]

    # contiuous variables
    cont_cols = ["age",
                 "duration"
                 ]

    file = open(b"svm.pickle", 'rb')
    prepro_dict = pickle.load(file)

    del prepro_dict["enc_dict"]["y"]

    df_valid = pd.read_excel("data/test_file.xlsx")

    m = df_valid.eval("housing == 'unknown'")

    preprocess(df_valid.loc[~m], cat_cols, cont_cols, target_col=None, rand_seed=667, \
               enc_dict=prepro_dict["enc_dict"], scaler_cont=prepro_dict["scaler_cont"], \
               scaler_ord=prepro_dict["scaler_ord"], dummy_coding=True, mc_kernel=prepro_dict["mc_kernel"])

import os
import pickle

from typing import List, Dict, Tuple

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

from collections import Counter

from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

from imblearn.over_sampling import SMOTE, SVMSMOTE, SMOTEN, SMOTENC
from imblearn.over_sampling import RandomOverSampler

from sklearn.metrics import accuracy_score
from sklearn.inspection import permutation_importance
from sklearn.metrics import RocCurveDisplay
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from sklearn import metrics

from sklearn.model_selection import GridSearchCV

import prepro

MODEL_PATH = "./models"


def plot_feature_importance_rf(df_train: pd.DataFrame, df_test: pd.DataFrame, cont_cols, cat_cols, rand_seed:int=667) -> None:
    """
    Use random forest classification to probe feature importance.
    Plots data with features on y axes and classification impact on x axis.

    :param df_train: train set
    :param df_test: test set
    :param cont_cols: continuous columns
    :param cat_cols: continuous columns
    :param rand_seed: random seed
    :return: None
    """

    # preprocess training set
    X_train, y_train, enc_dict, scaler_cont, scaler_ord, mc_kernel = prepro.preprocess(
        df_train, target_col="y", cat_cols=cat_cols, cont_cols=cont_cols, rand_seed=rand_seed,
        enc_dict=None, scaler_cont="zscore", scaler_ord=None, mc_kernel=None, dummy_coding=False
    )

    # preprocess test set
    X_test, y_test, enc_dict, _, _, _ = prepro.preprocess(
        df_test, target_col="y", cat_cols=cat_cols, cont_cols=cont_cols, rand_seed=rand_seed,
        enc_dict=enc_dict, scaler_cont=scaler_cont, scaler_ord=scaler_ord, mc_kernel=mc_kernel, dummy_coding=False
    )

    # resample
    ros = RandomOverSampler(random_state=rand_seed)
    X_res, y_res = ros.fit_resample(X_train, y_train)

    # fit random forest
    rf = RandomForestClassifier(random_state=rand_seed)
    rf.fit(X_res, y_res)

    # get feature importance via permutations
    result = permutation_importance(
        rf, X_test, y_test, n_repeats=10, random_state=rand_seed, n_jobs=2
    )

    # prepare for plotting
    sorted_importances_idx = result.importances_mean.argsort()
    importances = pd.DataFrame(
        result.importances[sorted_importances_idx].T,
        columns=X_test.columns[sorted_importances_idx],
    )
    ax = importances.plot.box(vert=False, whis=10)
    ax.set_title("Permutation Importances (test set)")
    ax.axvline(x=0, color="k", linestyle="--")
    ax.set_xlabel("Decrease in accuracy score")
    ax.figure.tight_layout()


def train_classifier(model_str, df_train, df_test=None, cont_cols=None, cat_cols=None, grid_search=False, persist=False, rand_seed=667):
    """
    Train a classifier

    :param model_str:
    :param df_train:
    :param df_test:
    :param cont_cols:
    :param cat_cols:
    :param grid_search:
    :param persist:
    :param rand_seed:
    :return:
    """

    if model_str == "rf":
        # RANDOM FOREST
        # preprocess training set
        # do not dummy code
        # no ordinal scaling
        model = RandomForestClassifier()
        scaler_cont = "minmax"
        scaler_ord = "minmax"
        dummy_code = False
        ov_sampler = SVMSMOTE(random_state=rand_seed) #RandomOverSampler
        param_grid = {
            'bootstrap': [True],
            'max_depth': [30, 60, 90, 120],
            'max_features': [5, 10, 15],
            'min_samples_leaf': [4, 8, 10],
            'n_estimators': [200, 400, 800, 1600]
        }

    elif model_str == "svm":
        model = SVC(kernel="linear", probability=True) # switch on prob response for ROC calculation
        scaler_cont = "minmax"
        scaler_ord = None#"minmax"
        dummy_code = True
        ov_sampler = SMOTE(random_state=rand_seed)#RandomOverSampler

    elif model_str == "logreg":
        model = LogisticRegression(random_state=rand_seed, solver="liblinear", penalty="l2")
        scaler_cont = "minmax"
        scaler_ord = None#"minmax"
        dummy_code = False
        ov_sampler = SMOTE(random_state=rand_seed) #RandomOverSampler
        param_grid = {'penalty': ['l1', 'l2'],
         'C': np.logspace(-4, 4, 20),
         'solver': ['liblinear']}

    elif model_str == "lda":
        model = LinearDiscriminantAnalysis(shrinkage="auto", solver="lsqr")
        scaler_cont = "zscore"
        scaler_ord = "minmax"
        dummy_code = False
        ov_sampler = SMOTE(random_state=rand_seed)#RandomOverSampler

    # train imputer on all cols if we do not
    # know what test data is expected
    if df_test is None:
        mc_on_all_cols = True
        model_str = model_str+"_final"
    else:
        mc_on_all_cols = False

    X_train, y_train, enc_dict, scaler_cont, scaler_ord, mc_kernel = prepro.preprocess(
        df_train, target_col="y", cat_cols=cat_cols, cont_cols=cont_cols, rand_seed=rand_seed,
        enc_dict=None, scaler_cont=scaler_cont, scaler_ord=scaler_ord, mc_kernel=None, mc_on_all_cols=mc_on_all_cols, dummy_coding=dummy_code
    )

    # resample
    X_res, y_res = ov_sampler.fit_resample(X_train, y_train)

    print(Counter(y_res))

    # do grid search to find optimal hyper params
    if grid_search:
        # hyper param tuning
        print("Running grid search")
        grid = GridSearchCV(model, param_grid, cv=10, scoring='accuracy', return_train_score=False)
        grid.fit(X_res, y_res)

        return grid

    # preprocess test set if provided
    if df_test is not None:
        # preprocess test set
        X_test, y_test, enc_dict, _, _, _ = prepro.preprocess(
            df_test, target_col="y", cat_cols=cat_cols, cont_cols=cont_cols, rand_seed=rand_seed,
            enc_dict=enc_dict, scaler_cont=scaler_cont, scaler_ord=scaler_ord, mc_kernel=mc_kernel, dummy_coding=dummy_code
        )

        # provide missing columns in case not all feature levels occurred in train and test set
        # fill missing dummy cols with 0s
        if dummy_code is not None:
            X_res, X_test = prepro.insert_missing_cols(X_res, X_test)

    # fit classifier
    model.fit(X_res, y_res)

    # evaluate classifier if test set is provided
    if df_test is not None:
        # predict
        y_pred = model.predict(X_test)

        # plot evaluation metrics
        evaluate_classifier(model, y_pred, X_res, y_res, X_test, y_test)

    # store column information in dedicated dict
    col_dict = {
        "feat_cols": X_res.columns,
        "cont": cont_cols,
        "cat": cat_cols,
    }

    # persist model on HDD
    if persist:
        if not os.path.isdir(MODEL_PATH):
            os.mkdir(MODEL_PATH)

        save_dict = {
            "model": model,
            "enc_dict": enc_dict,
            "scaler_cont": scaler_cont,
            "scaler_ord": scaler_ord,
            "dummy_coding": dummy_code,
            "mc_kernel": mc_kernel,
            "col_dict": col_dict
        }

        f = open(f"{MODEL_PATH}/{model_str}.pickle", "wb")
        pickle.dump(save_dict, f)

    return model, enc_dict, scaler_cont, scaler_ord, mc_kernel, col_dict


def predict_from_dict(model_dict: Dict, df_test: pd.DataFrame, rand_seed: int=667) -> np.array:
    """
    Wrapper function to predict new targets based on a previously trained model and its
    corresponding preprocessing options.

    :param model_dict:
    :param df_test:
    :param rand_seed:
    :return:
    """

    model = model_dict["model"]
    cat_cols = model_dict["col_dict"]["cat"]
    cont_cols = model_dict["col_dict"]["cont"]
    feat_cols = model_dict["col_dict"]["feat_cols"]
    enc_dict = model_dict["enc_dict"]
    scaler_cont = model_dict["scaler_cont"]
    scaler_ord = model_dict["scaler_ord"]
    mc_kernel = model_dict["mc_kernel"]
    dummy_code = model_dict["dummy_coding"]

    print(f"Predicting with {type(model).__name__} ...")

    X_test, _, _, _, _, _ = prepro.preprocess(df_test, cat_cols, cont_cols, target_col=None, rand_seed=rand_seed,
                                             enc_dict=enc_dict, scaler_cont=scaler_cont,
                                             scaler_ord=scaler_ord, dummy_coding=dummy_code,
                                             mc_kernel=mc_kernel)

    return model.predict(X_test[feat_cols])


def evaluate_classifier(model: "ModelObject", y_pred: np.array, X_train: pd.DataFrame, y_train: np.array, X_test: pd.DataFrame, y_test: np.array, plot_conf_mat: bool=False):
    """
    Plots ROC curves and optimal tradeoffs between true positive and false positive rates.

    :param model: model object of previously trained classifier
    :param y_pred: array with binary predictions
    :param X_train: dataframe with training features
    :param y_train: binary class labels corresponding to X_train
    :param X_test: dataframe with test features
    :param y_test: binary class labels corresponding to X_test
    :param plot_conf_mat: shall we plot a confusion matrix for 1 if p(y==1) > .5 ?
    :return:
    """

    # --- plot entire ROC curves
    print("plotting ROC ...")
    fig, ax = plt.subplots()
    # train
    RocCurveDisplay.from_estimator(model, X_train, y_train, pos_label=1, ax=ax)
    # test
    RocCurveDisplay.from_estimator(model, X_test, y_test, pos_label=1, ax=ax)
    ax.legend(["train", "test"])

    # --- mark best tpr fpr tradeoff by "*"
    # train
    y_train_probs = model.predict_proba(X_train)[:,1]
    fpr, tpr, thresholds = metrics.roc_curve(y_train, y_train_probs, pos_label=1)
    ind = np.argmax(tpr - fpr)
    plt.plot(fpr[ind], tpr[ind], "*")
    plt.text(fpr[ind], tpr[ind], f"tpr:{tpr[ind]: 0.2f}; fpr:{fpr[ind]: 0.2f}; thresh: {thresholds[ind]: 0.2f}")

    # test
    y_test_probs = model.predict_proba(X_test)[:, 1]
    fpr, tpr, thresholds = metrics.roc_curve(y_test, y_test_probs, pos_label=1)
    ind = np.argmax(tpr - fpr)
    plt.plot(fpr[ind], tpr[ind], "*")
    plt.text(fpr[ind], tpr[ind], f"tpr:{tpr[ind]: 0.2f}; fpr:{fpr[ind]: 0.2f}; thresh: {thresholds[ind]: 0.2f}")

    plt.title(type(model).__name__)

    if plot_conf_mat:
        # confusion matrix
        print("plotting confusion matrix ...")
        fig, ax = plt.subplots()
        #cm = confusion_matrix(y_test, y_pred, labels=model.classes_, normalize="true")
        cm = confusion_matrix(y_test, y_pred, labels=model.classes_, normalize=None)
        disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=model.classes_)
        disp.plot(ax=ax)

    # accuracy
    print(accuracy_score(y_test, y_pred))


if __name__ == '__main__':
    print("Main goes here")
# customer_classification



## Summary

This repository contains the code for the customer classification assignment provided
as a technical exercise.

"./data" contains training / test and prediction data
"./models" contains persisted models

"prepro.py" contains everything needed for preprocessing the data.

"classification.py" contains everything that is needed for classification and model
evaluation.

"EDA.ipynb" Serves as an entry point where you also can find an exploratory data analysis (EDA)